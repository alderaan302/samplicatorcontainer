# samplicatorcontainer

## Summary

The samplicatorcontainer runs an ubuntu image with samplicator installed

## Build

# clone git repo

From the directory your containers live in (in my case it is /mnt) run these commands:

```
git clone git@gitlab.com:alderaan302/samplicatorcontainer.git
cd samplicatorcontainer
docker build . -t samplicatorContainer
```

## Export container image (optional)
 
If you need to export the container image to another host that possibly does not have internet run this command:

```
docker save samplicatorcontainer > samplicatorImage.tar
tar -cvf ~/samplicatorContainerFiles.tar *
```

## Import the container image and files on another host

Transfer your ~/samplicatorContainerFiles.tar to the folder on the remote host that willl contain the container and run these commands:

```
tar -xvf samplicatorContainerFiles.tar
cd samplicatorcontainer
docker load < samplicatorImage.tar
```

## Running the container

The samplicator.conf file is in etc/samplicator. The sample listens on port 162 and redirects traffic to 192.168.10.14 on port 10162 and 192.168.10.190 and .141 on port 162. Note that you don't want to listen and send to yourself on the same port. If you need to change the listening port, see the ports section in the docker-compose.yml.run file. To start the container run this command:

```
docker-compose -f docker-compose.yml.run up -d 
```
