FROM ubuntu:latest
EXPOSE 162/udp
WORKDIR /app
ADD bin/samplicator_1.3.6-1_amd64.deb /app
RUN apt-get update
RUN dpkg -i samplicator_1.3.6-1_amd64.deb
RUN apt install tcpdump snmp snmpd -y
ENTRYPOINT ["/usr/local/bin/samplicate"]

